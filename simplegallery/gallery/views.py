from django.views.generic import ListView, DetailView
from django.http import HttpResponse

from models import Image
import simplejson as json

class GalleryView(ListView):
    template_name = "gallery/list.html"
    model = Image

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset().filter(
                               title__icontains = request.GET.get('query', '')
                           )
        allow_empty = self.get_allow_empty()
        
        if request.GET.has_key('format'):
            format = request.GET.get('format')
            if format == 'json':
                return HttpResponse(
                  json.dumps(str(self.object_list.values())),
                  mimetype="application/json"
                )
        
        context = self.get_context_data(object_list=self.object_list)
        return self.render_to_response(context)
        
class GalleryDetailView(DetailView):
    template_name = "gallery/detail.html"
    model = Image

class GalleryByAuthorView(ListView):
    model = Image
    template_name='gallery/list.html'
    
    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset().filter(
                               author__id = kwargs['authorname']
                           )
                           
        allow_empty = self.get_allow_empty()
        context = self.get_context_data(object_list=self.object_list)
        return self.render_to_response(context)

